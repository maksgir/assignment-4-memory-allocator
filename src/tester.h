

#ifndef MEMORY_ALLOCATOR_TESTER_H
#define MEMORY_ALLOCATOR_TESTER_H

#include "mem.h"
#include "mem_internals.h"

#define TESTS_COUNT 5

bool simpleSucceedAllocateTest(void);
bool freeOneOfManyBlocksTest(void);
bool freeTwoOfManyBlocksTest(void);
bool growHeapTest(void);
bool changeBlockAddrTest(void);


#endif //MEMORY_ALLOCATOR_TESTER_H
