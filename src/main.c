#include "tester.h"

int main(){
    int good = 0;

    good += simpleSucceedAllocateTest();
    good += freeOneOfManyBlocksTest();
    good += freeTwoOfManyBlocksTest();
    good += growHeapTest();
    good += changeBlockAddrTest();
    printf("Passes %d of %d tests\n", good, TESTS_COUNT);
}
