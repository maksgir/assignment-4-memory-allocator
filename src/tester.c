#define _DEFAULT_SOURCE

#include "tester.h"

#define HEAP_SIZE 5000


static void heap_free(void *heap) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = HEAP_SIZE}).bytes);
}

bool simpleSucceedAllocateTest() {
    printf("Test: simpleSucceedAllocateTest started!\n");
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *array = _malloc(1000);
    debug_heap(stdout, heap);

    if (!array) {
        printf("FAILED\n");
        return false;
    }

    _free(array);
    heap_free(heap);

    printf("PASSED\n");
    return true;

}

bool freeOneOfManyBlocksTest() {
    printf("Test: freeOneOfManyBlocksTest started!\n");
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *data1 = _malloc(1000);
    debug_heap(stdout, heap);

    void *data2 = _malloc(1000);
    debug_heap(stdout, heap);

    void *data3 = _malloc(1000);
    debug_heap(stdout, heap);

    if (!data1 || !data2 || !data3) {
        printf("Blocks weren't initialized!\n");
        return false;
    }

    _free(data2);

    if (!data1 || !data3) {
        printf("Unexpected blocks were free !\n");
        printf("Test: freeOneOfManyBlocksTest started!\n");
        return false;
    }

    debug_heap(stdout, heap);

    _free(data1);
    _free(data3);
    heap_free(heap);
    printf("PASSED\n");
    return true;

}

bool freeTwoOfManyBlocksTest() {
    printf("Test: freeTwoOfManyBlocksTest started!\n");
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *data1 = _malloc(1000);
    debug_heap(stdout, heap);

    void *data2 = _malloc(1000);
    debug_heap(stdout, heap);

    void *data3 = _malloc(1000);
    debug_heap(stdout, heap);

    if (!data1 || !data2 || !data3) {
        printf("Blocks weren't initialized!\n");
        return false;
    }

    _free(data2);
    _free(data3);

    if (!data1) {
        printf("Unexpected blocks were free !\n");
        return false;
    }

    debug_heap(stdout, heap);
    _free(data1);
    heap_free(heap);
    printf("PASSED\n");
    return true;

}

bool growHeapTest() {
    printf("Test: growHeapTest started!\n");
    bool flag = false;
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *huge_block = _malloc(HEAP_SIZE * 3);
    debug_heap(stdout, heap);

    struct block_header const *header = heap;

    if (header->capacity.bytes > HEAP_SIZE) {
        printf("Heap capacity has risen\n");
        flag = true;
    }

    _free(huge_block);
    heap_free(heap);
    return flag;

}

bool changeBlockAddrTest() {
    printf("Test: changeBlockAddrTest started!\n");
    bool flag = false;
    void *heap = heap_init(HEAP_SIZE);

    debug_heap(stdout, heap);

    void *block1= _malloc(1000);

    debug_heap(stdout, heap);

    if (!block1) {
        printf("Blocks weren't initialized!\n");
        return false;
    }

    struct block_header const *header = (struct block_header*) (block1 - offsetof(struct block_header, contents));

    (void) mmap((void *) header->contents + header->capacity.bytes, 2000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void*block2 = _malloc(HEAP_SIZE*2);

    debug_heap(stdout, heap);


    if (!block2) {
        printf("Blocks weren't initialized!\n");
        return false;
    }

    _free(block1);
    _free(block2);
    heap_free(heap);

    return flag;

}
